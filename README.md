## Notes
The assignement is done using iOS and is architected to be completely testable, i.e business logic, navigation, data transformation. Some shortcuts have been taken, but most of them are marked as TODO in the project.
There are two major screen modules -
- Launches: The current home screen which shows the list of launches
- LaunchDetails: The screen which shows the details for the lauch, it only needs the launch id to do so.

Sample Video is attached, check the Sample.mov file

### Date
12th Oct 2021
### Instructions for how to build & run the app
To build the app one needs 
- **Xcode - Version 12.4 (12D4e)** can be downloaded from   [here](https://developer.apple.com/services-account/download?path=/Developer_Tools/Xcode_12.4/Xcode_12.4.xip)
  - Swift package manager, which would be part of the xcode itself, so nothing needed there.
- **Cocoapods - Version 1.10.1** can be installed using the following commands ```sudo gem install cocoapods -v 0.25.0```.
  - From the project root folder run the command `pod install`
- Next open the **Launches.xcworkspace** not Launches.xcodeproj
- Select any simulator and then hit the **Play** button or **cmd + r**
### Time spent
Total - 12 hours
- UI research - 2 hours
- App architeture design - 1 hours
- **Coding the assignment - 7 hours**
- miscellaneous - 2
### Assumptions made
The app is made with the following assumptions -
- The app does not requires any login as the information is available in public domain
- The app data is not encrypted for the same reason.
- As the focus of the app was to show the technical skills, so some redundat works are skipped.
- Because of short time a lot of key features were left and only the essential were selected.
 
### Assume your application will go into production...
1. What would be your approach to ensuring the application is ready for production (testing)?
    - Unit tests are done for Launches module.
    - All the flows, business logic, navigation, data transformation are fully testable.
    - All the modules uses dependency injection to load any screen. In other words any screen can be opened from anywhere or the screens are fully reusable.
2. How would you ensure a smooth user experience as 1000s of users start using your app simultaneously?
    - The project is based on Highly scalacle architecture RIBs, which can enable not only building the app for the masses but also opens the door for lots of experimentation
    - The modular screen and core components make it easy to do any changes as required for the users/ features.
    - Any new requirements can be incorporated easily and Unit Tests will prevent regression.
3. What key steps would you take to ensure application security?
    - Any sensitive data, (like password, personal identfiable information, health related data etc) would be stored in secure keychain, it keeps the data encrypted.
    - All API must be HTTPS, and if needed use authorized API
    - The auth token should not be valid for indefinitely long time, but should expire. And it should be renewed automatically if someone is using the app regularly
    - If we are storing large amount of sensitive data then we must encrypt the data at db level.
    - Minimise 3rd party library usages, and be mindful about their usages.
    - Perform penetration testing.
### Shortcuts/Compromises made
There has been many shortcuts taken into this sample demo app 😀. Some of these are -
- There is no root screen module, but only a root view controller which directly initializes the first screen. But the root controller is still made to demostrate how we can leverage it to switch the first screen anytime.
- GraphQL service layer is not mocked, because of time contraints
- The UI are mostly designed from Storyboards instead of code. In actual product code would have been used
- Localization of string is not done

##### In an actual production app, it would be a nice to incorporate a few more thing
- Theming of the app
- Analytics framework for the buttons, impressions, screen loads etc.
- Log the errors on server
- UI testing can also be added.
- Push notifications for better engagement
- Linting frameworks like SwiftLint would have been used.

### Why did you choose the specific technology/patterns/libraries?
- iOS was choosen because it is the tech stack where I can demostrate my tech skills the most.
- **RIBs Architecture** is used, but without the help of the framework.
    - It enables one to use scalable app, which can support **hundreds of developers** to work simultaneously and build an app for **billions of users**
    - It helps to make everything testable, including routing logic, business logic, data transformation etc.
    - Features control and rollout are exceptionally easy
    - Almost anything is reusable, even the entire screens. For instance, screen navigation flow change is very simple
- Scripting (Sourcery) is used to generate the mock (swift) files instead of generating them manually. Any change in the actual implementaion would also update the mock. Reduces the manual work, gives speed and no changes of error. Cocoa pods is used to install Suoucery
- Apollo library is used for GraphQL, as it serves multiple use cases
    - Servers as a network services for API calls
    - Provides support for model code generation
    - Local data saving/ caching can be done easily
### Stretch goals attempted
All the applicable stretched goals are done.
- Offline support **[done]**
- Deeplinks **[done]**
- Use Navigator 2.0 **[not done]**, as it's a Flutter component not applicable on iOS
### Other information about your submission that you feel it's important that we know.
- The Launches screen UI is highly configurable, for eg
    - It can be configured to display 1, 2, 3 or any number of times in a row
    - It can scroll vertically or horizontally
- The architecture is highly scalable, for instance, the support for pagination or pull to refresh can be easily added.
- Care has been taken to use dependency injection to fullest.
- Performance consideration are accounted.
- Code is reusable to the extent that any screen can be presented from any place
    - Same screen layout can be powered to show different data and handle different UI
    - All the servies are reusable
- String hardcoding for configurable data is zero
- Image caching exists
- Efforts have been made to make the UI reorder within a screen as simple dragging the visual components, and you are done.
- Protocol oriented approach is taken.
- Errors like network failure or no data are presented to the users

### Your feedback on this technical challenge
Mainly a very good and interesting challenge. The questions were answered over the email. One improvement could be to update the assignment wording to use generic keywords like Architecure, as when it suggests to use BLoC architecture or to use Navigator 2.0, as examples, it gives a feeling that this assignment must be done in FLutter. And I must say Sara clarified these questions for me, Kudos to her and the team on clarification and such fast reply.
