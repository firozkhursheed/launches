//
//  Storyboards.swift
//  Launches
//
//  Created by Firoz Khursheed on 10/10/21.
//

import UIKit

/// Contains helper functions to get references of storyboard. This also reduces the changes of mistakes
struct Storyboards {
  static let mainStoryboard = UIStoryboard(name: "Main", bundle: nil) // TODO: Use script to generate this file, that way there won't be need to update this file anytime a storybord is added or removed
}
