//
//  AlertView.swift
//  Launches
//
//  Created by Firoz Khursheed on 10/10/21.
//

import UIKit

final class AlertView {

  // A simple helper method to present errors to the users
  // TODO: Localize the string here
  static func showErrorAlert(title: String = "Error",
                      message: String,
                      on viewController: UIViewController) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
      // We are not handling anything when error happens in the demo app,
      // To handle the error, we would return the closure on the tap of OK button
    }))
    viewController.present(alert, animated: true, completion: nil)
  }
}
