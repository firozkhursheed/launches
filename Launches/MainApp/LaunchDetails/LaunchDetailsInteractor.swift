//
//  LaunchDetailsInteractor.swift
//  Launches
//
//  Created by Firoz Khursheed on 10/10/21.
//

import Foundation

/// A protocol for the presenter (ViewController), this ensures Unit testing can be done easily and any other presenter can also use be made to use the same interactor if required. Very useful incase of refactoring.
protocol LaunchDetailsPresentable: AutoMockable {
  var listener: LaunchDetailsPresentableListener? { get set }
  func show(details: LaunchDetailsViewModel)
  func show(error: String)
}

// All the Error related to Luanch Details Data can be tracked at single place
enum LaunchDataError: Error {
  case noLaunchData
}

/// Controls the business logic for the module/ screen. Interactors are testable. They initialize the ViewModels which transforms the data and are used by VC as data source.
final class LaunchDetailsInteractor: LaunchDetailsPresentableListener {

  private var viewModel: LaunchDetailsViewModel?
  private var presenter: LaunchDetailsPresentable
  private let services: GraphQLService

  /// Dependencies for this interactor are injected in init
  init(services: GraphQLService, presenter: LaunchDetailsPresentable) {
    self.services = services
    self.presenter = presenter
    self.presenter.listener = self
  }

  // The launch details screen can be presented form anywhere,
  // i.e. either form Launches screen or from deep links.
  // So it supports 2 ways of loading the View Model
  // as it's not a good practice to pass all the launches data to details screen which would show the details for a singe Launch

  /// Supports load by model
  func load(model: LaunchDetailsViewModel) {
    viewModel = model

    if let vm = viewModel {
      presenter.show(details: vm)
    } else {
      presenter.show(error: "No information found") // TODO: Localization
    }
  }

  // Supports load by LaunchID
  func load(id: LaunchID) {
    getLaunch(by: id)
  }

  /// Gets the Launch details by the LaunchID
  private func getLaunch(by id: LaunchID ) {
    // Sets the query parameter
    let query = LaunchQuery(id: id)

    // API call on background thread to prevent UI block
    services.fetch(query: query) { [weak self] result in
      // Memory leaks are prevented using weak self here

      // The result is returned on the main thread, where the UI tasks can be done
      switch result {

      case .success(let graphQLResult):
        // On success if the launch data exits, show the details
        if let launch = graphQLResult.data?.launch {
          let detailsVM = LaunchDetailsViewModel(launch: Launch(unsafeResultMap: launch.resultMap))
          self?.load(model: detailsVM)
        } else {
          // No launch data so the error, so that the user is not left in limbo state
          self?.presenter.show(error: LaunchDataError.noLaunchData.localizedDescription)
        }

      case .failure(let error):
        // Error case, update the user
        self?.presenter.show(error: error.localizedDescription)
      }
    }
  }
}
