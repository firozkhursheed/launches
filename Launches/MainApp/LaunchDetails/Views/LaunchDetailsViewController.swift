//
//  LaunchDetailsViewController.swift
//  Launches
//
//  Created by Firoz Khursheed on 10/10/21.
//

import UIKit
import Kingfisher

/// To whosoever is listening to this Viewcontrollers actions gets to know what the user actions are.
protocol LaunchDetailsPresentableListener: AnyObject, AutoMockable {
  // TODO: Declare properties and methods that the view controller can invoke to perform
  // business logic, such as bookmark(). This protocol is implemented by the corresponding
  // interactor class.
}

/// View controllers, perform the actions asked by the Interactor and uses the transformed data from the view model. ViewControllers were not designed to be unit tested, so we are keeping them simple without any logic.
final class LaunchDetailsViewController: UIViewController {

  // MARK: - IBOutlet
  /// Private outlets so that they can not be accessed from outside
  @IBOutlet private weak var contentImageView: UIImageView!
  @IBOutlet private weak var tableView: UITableView!

  // MARK: - Properties
  weak var listener: LaunchDetailsPresentableListener?

  private let reuseIdentifier = "LaunchDetailsCellIdentifier"
  private var viewModel: LaunchDetailsViewModel?
}

// All the logic related to different flow are put into their corresponding extensions block. This makes the code navigation logical and easy

// MARK: - LaunchDetailsPresentable
extension LaunchDetailsViewController: LaunchDetailsPresentable {
  func show(details: LaunchDetailsViewModel) {
    viewModel = details
    contentImageView.kf.indicatorType = .activity
    contentImageView.kf.setImage(with: details.imageSource)
    tableView.reloadData()
  }

  func show(error: String) {
    AlertView.showErrorAlert(message: error, on: self)
  }
}

// MARK: - UITableViewDataSource
extension LaunchDetailsViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    viewModel?.rowCount ?? 0
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let detailsCell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath)
 as? LaunchDetailsTableViewCell else {
      fatalError("LaunchDetailsTableViewCell unwrapping failed")
    }

    detailsCell.titleLabel.text = viewModel?.title
    detailsCell.detailslabel.text = viewModel?.details
    detailsCell.rocketNameLabel.text = viewModel?.rocketName
    detailsCell.launchYearLabel.text = viewModel?.launchDate
    return detailsCell
  }
}
