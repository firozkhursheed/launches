//
//  LaunchDetailsTableViewCell.swift
//  Launches
//
//  Created by Firoz Khursheed on 10/10/21.
//

import UIKit

final class LaunchDetailsTableViewCell: UITableViewCell {

  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var rocketNameLabel: UILabel!
  @IBOutlet weak var launchYearLabel: UILabel!
  @IBOutlet weak var detailslabel: UILabel!

}
