//
//  LaunchDetailsBuilder.swift
//  Launches
//
//  Created by Firoz Khursheed on 10/10/21.
//

import UIKit

/// Builder as the name suggestes, buids the flow, sets any dependencies. It acts a single point for interaction for entering into any flow. DI
/// In actual project these would also be supported by protocols
final class LaunchDetailsBuilder {
  // Self dependencies
  private var services: GraphQLService = Network.shared.apollo
  private var interactor: LaunchDetailsInteractor?
  private(set) var viewControler: LaunchDetailsViewController?

  init() {     // Set any outer dependencies here
  }

  func build() -> LaunchDetailsInteractor {
    guard let vc = Storyboards.mainStoryboard.instantiateViewController(withIdentifier: "LaunchDetailsStoryboardID") as? LaunchDetailsPresentable else {

      // In actual project, there would be a class doing this task, so it would be be required to add this in each builder
      fatalError("LaunchDetailsStoryboardID not found")
    }

    let launchDetailsInteractor = LaunchDetailsInteractor(services: services, presenter: vc)
    interactor = launchDetailsInteractor
    viewControler = vc as? LaunchDetailsViewController
    return launchDetailsInteractor
  }

}
