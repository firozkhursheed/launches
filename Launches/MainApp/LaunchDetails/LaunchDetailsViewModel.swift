//
//  LaunchDetailsViewModel.swift
//  Launches
//
//  Created by Firoz Khursheed on 10/10/21.
//

import Foundation

struct LaunchDetailsViewModel {
  private let launch: Launch
  private(set) var id: LaunchID
  private(set) var title: String
  private(set) var imageSource: URL
  private(set) var launchDate: String
  private(set) var rocketName: String
  private(set) var details: String
  private(set) var rowCount: Int

  /// The ViewModel is fully testable using this approach
  init(launch: Launch) {
    guard let launchID = launch.id else { fatalError(LaunchListDataError.noID.localizedDescription)}

    guard let title = launch.missionName else { fatalError(LaunchListDataError.noMissionName.localizedDescription)}
    let stringURL = launch.links?.flickrImages?.first as? String
            ?? ConstantStrings.noImage.rawValue
    guard let url = URL(string: stringURL) else { fatalError(ParsingError.stringToUrlParsingFail.localizedDescription) }
    guard let date = launch.launchDateUtc else { fatalError(LaunchListDataError.noLaunchDate.localizedDescription) }
    guard let rocketName = launch.rocket?.rocketName else { fatalError(LaunchListDataError.noRocketName.localizedDescription) }
    guard let details = launch.details else { fatalError(LaunchListDataError.noDetails.localizedDescription) }

    self.launch = launch
    self.id = launchID
    self.title = title
    self.imageSource = url
    self.launchDate = "🗓 " + LaunchDetailsViewModel.dateFrom(utcString: date)
    self.rocketName = "🚀 " + rocketName
    self.details = details
    self.rowCount = 1
  }

  private static func dateFrom(utcString: String) -> String {
    let dateFormatTemplate = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'" // TODO: Make this standard format and use
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = dateFormatTemplate
          dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
          let date = dateFormatter.date(from: utcString)

    dateFormatter.dateFormat = "MMM yyyy"
          dateFormatter.timeZone = NSTimeZone.local
          let timeStamp = dateFormatter.string(from: date!) // TODO: Handle failure case
    print(timeStamp)
    return timeStamp
  }
}
