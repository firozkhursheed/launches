//
//  LaunchesRouter.swift
//  Launches
//
//  Created by Firoz Khursheed on 10/10/21.
//

import UIKit

/// The interactor conforms to this protocol to know about router
protocol LaunchesInteractable: AutoMockable {
    var router: LaunchesRoutering? { get set }
}

/// Router communicate to the controller using this protocol
protocol LaunchesViewControllable: ViewControllerable, AutoMockable {
  func present(vc: UIViewController)
}

/// All the routing logic from Launches screen to any other screen is carved out here. The interactor does not need to know how to present, but only what to present
class LaunchesRouter {
  private let launchDetailsBuilder = LaunchDetailsBuilder()
  private let viewController: LaunchesViewControllable
  private var interactor: LaunchesInteractable

  init(interactor: LaunchesInteractable,
       viewController: LaunchesViewControllable) {
    self.viewController = viewController
    self.interactor = interactor
    self.interactor.router = self
  }

}

// MARK: - LaunchesRoutering
extension LaunchesRouter: LaunchesRoutering {
  func routeToLaunchDetails(id: LaunchID) {
    let launchDetailsInteractor = launchDetailsBuilder.build()
    presentLaunchDetails()
    launchDetailsInteractor.load(id: id)
  }

  func routeToLaunchDetails(model: LaunchDetailsViewModel) {
    let launchDetailsInteractor = launchDetailsBuilder.build()
    presentLaunchDetails()
    launchDetailsInteractor.load(model: model)
  }

  private func presentLaunchDetails() {
    guard let vc = launchDetailsBuilder.viewControler else {
      fatalError("LaunchDetails ViewController is missing")
    }
    viewController.present(vc: vc)
  }
}
