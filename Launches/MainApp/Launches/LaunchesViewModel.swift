//
//  LaunchesViewModel.swift
//  Launches
//
//  Created by Firoz Khursheed on 10/10/21.
//

import Foundation
import UIKit

/// View Model powering the Launches screen
struct LaunchesViewModel {
  var items: [LaunchCellViewModel] = []

  let sectionInsets = UIEdgeInsets(
    top: 10.0,
    left: 20.0,
    bottom: 20.0,
    right: 20.0)

  let sectionLineSpacing: CGFloat = 24.0

  var pagination = Pagination(limit: 10, offset: 0)
}

/// Pagination is separated so that if required it can be extended separately and can be used with other files also which needs pagination
struct Pagination {
  let limit: Int
  let offset: Int
  let order = "desc"
}
