//
//  LaunchCellViewModel.swift
//  Launches
//
//  Created by Firoz Khursheed on 10/10/21.
//

import Foundation

struct LaunchCellViewModel {
  private(set) var id: LaunchID
  private(set) var title: String
  private(set) var imageSource: URL

  private let launch: Launch

  /// The ViewModel is fully testable using this approach
  init(launch: Launch) {
    guard let launchID = launch.id else { fatalError(LaunchListDataError.noID.localizedDescription)}

    guard let title = launch.missionName else { fatalError(LaunchListDataError.noMissionName.localizedDescription)}
    let stringURL = launch.links?.flickrImages?.first as? String
            ?? ConstantStrings.noImage.rawValue
    guard let url = URL(string: stringURL) else { fatalError(ParsingError.stringToUrlParsingFail.localizedDescription) }

    self.launch = launch
    self.id = launchID
    self.title = title
    self.imageSource = url
  }
}
