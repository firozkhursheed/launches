//
//  ViewController.swift
//  Launches
//
//  Created by Firoz Khursheed on 10/10/21.
//

import UIKit

/// To whosoever is listening to this Viewcontrollers actions gets to know what the user actions are.
protocol LaunchesPresentableListener: AnyObject, AutoMockable {
  func showDetails(id: LaunchID)
}

/// View controllers, perform the actions asked by the Interactor and uses the transformed data from the view model. ViewControllers were not designed to be unit tested, so we are keeping them simple without any logic.
final class LaunchesViewController: UIViewController {

  @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
  @IBOutlet private weak var collectionView: UICollectionView!

  weak var listener: LaunchesPresentableListener?

  // MARK: - Properties
  private var viewModel = LaunchesViewModel()

  private let reuseIdentifier = "LaunchCell"
  private let itemsPerRow: CGFloat = 1
}

// All the logic related to different flow are put into their corresponding extensions block. This makes the code navigation logical and easy

// MARK: - LaunchesPresentable
extension LaunchesViewController: LaunchesPresentable {
  func show(launches: LaunchesViewModel) {
    viewModel = launches
    collectionView.reloadData()
  }

  func show(error: String) {
    AlertView.showErrorAlert(message: error, on: self)
  }

  func showActivityIndicator() {
    activityIndicator.startAnimating()
  }

  func hideActivityIndicator() {
    activityIndicator.stopAnimating()
  }
}

// MARK: - UICollectionViewDataSource
extension LaunchesViewController: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return viewModel.items.count
  }

  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    guard let launchCell = collectionView.dequeueReusableCell(
      withReuseIdentifier: reuseIdentifier,
      for: indexPath) as? LaunchCollectionViewCell else {
      fatalError("LaunchCollectionViewCell unwrapping failed")
    }

    let cellViewModel = viewModel.items[indexPath.row] // TODO: Check index out of bound
    launchCell.set(imageFrom: cellViewModel.imageSource)
    launchCell.set(title: cellViewModel.title)
    return launchCell
  }
}

// MARK: - UICollectionViewDelegate
extension LaunchesViewController: UICollectionViewDelegate {
  func collectionView(_ collectionView: UICollectionView,
                               didSelectItemAt indexPath: IndexPath) {
    let selectedVM = viewModel.items[indexPath.row] // TODO: Do nil check, and this logic needs to be part of VM
    listener?.showDetails(id: selectedVM.id)
  }

}

// MARK: - Collection View Flow Layout Delegate
extension LaunchesViewController: UICollectionViewDelegateFlowLayout {
  func collectionView(
    _ collectionView: UICollectionView,
    layout collectionViewLayout: UICollectionViewLayout,
    sizeForItemAt indexPath: IndexPath
  ) -> CGSize {
    let paddingSpace = viewModel.sectionInsets.left * (itemsPerRow + 1)
    let availableWidth = view.frame.width - paddingSpace
    let widthPerItem = availableWidth / itemsPerRow
    return CGSize(width: widthPerItem, height: widthPerItem)
  }

  func collectionView(
    _ collectionView: UICollectionView,
    layout collectionViewLayout: UICollectionViewLayout,
    insetForSectionAt section: Int
  ) -> UIEdgeInsets {
    return viewModel.sectionInsets
  }

  func collectionView(
    _ collectionView: UICollectionView,
    layout collectionViewLayout: UICollectionViewLayout,
    minimumLineSpacingForSectionAt section: Int
  ) -> CGFloat {
    return viewModel.sectionLineSpacing
  }
}

// MARK: - LaunchesViewControllable
extension LaunchesViewController: LaunchesViewControllable {
  func present(vc: UIViewController) {
    present(vc, animated: true, completion: nil)
  }
}
