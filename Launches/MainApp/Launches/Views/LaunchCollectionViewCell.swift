//
//  LaunchCollectionViewCell.swift
//  Launches
//
//  Created by Firoz Khursheed on 10/10/21.
//

import UIKit
import Kingfisher

final class LaunchCollectionViewCell: UICollectionViewCell {

  /// Outlets are private
  @IBOutlet private weak var imageView: UIImageView!
  @IBOutlet private weak var titleLabel: UILabel!

  /// Helper function to set the image
  func set(imageFrom url: URL) {
    imageView.kf.indicatorType = .activity
    imageView.kf.setImage(with: url)
  }

  /// Helper function to set the image
  func set(title: String) {
    titleLabel.text = title
  }
}
