//
//  LaunchesBuilder.swift
//  Launches
//
//  Created by Firoz Khursheed on 10/10/21.
//

import UIKit

/// Builder as the name suggestes, buids the flow, sets any dependencies. It acts a single point for interaction for entering into any flow. DI
/// In actual project these would also be supported by protocols
final class LaunchesBuilder {
  // Self dependencies
  private var services: GraphQLService = Network.shared.apollo
  private var interactor: LaunchesInteractor?
  private(set) var viewControler: LaunchesViewController?

  init() {     // Set any outer dependencies here
  }

  func build() -> LaunchesInteractor {
    guard let vc = Storyboards.mainStoryboard.instantiateViewController(withIdentifier: "LaunchesStoryboardID") as? LaunchesPresentable else {

      // In actual project, there would be a class doing this task, so it would be be required to add this in each builder
      fatalError("LaunchesStoryboardID not found")
    }

    let launchInteractor = LaunchesInteractor(services: services, presenter: vc)
    interactor = launchInteractor
    viewControler = vc as? LaunchesViewController
    return launchInteractor
  }
}
