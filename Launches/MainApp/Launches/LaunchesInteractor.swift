//
//  LaunchesInteractor.swift
//  Launches
//
//  Created by Firoz Khursheed on 10/10/21.
//

import Foundation

/// A protocol for the presenter (ViewController), this ensures Unit testing can be done easily and any other presenter can also use be made to use the same interactor if required. Very useful incase of refactoring.
protocol LaunchesPresentable: LaunchesViewControllable, AutoMockable {
  var listener: LaunchesPresentableListener? { get set }
  func show(launches: LaunchesViewModel)
  func show(error: String)
  func showActivityIndicator()
  func hideActivityIndicator()
}

protocol LaunchesRoutering: AnyObject, AutoMockable {
  func routeToLaunchDetails(id: LaunchID)
  func routeToLaunchDetails(model: LaunchDetailsViewModel)
}

// The list of Data on Launces can be found at a single place, inside the interactor which controls the business logic
enum LaunchListDataError: Error {
  case noLaunchList
  case noLaunchData
  case noID
  case noMissionName
  case noImage
  case noLaunchDate
  case noRocketName
  case noDetails
}

enum ParsingError: Error {
  case stringToUrlParsingFail
}

/// Controls the business logic for the module/ screen. Interactors are testable. They initialize the ViewModels which transforms the data and are used by VC as data source.
final class LaunchesInteractor: LaunchesInteractable {

  private var viewModel = LaunchesViewModel()
  private var presenter: LaunchesPresentable
  private let services: GraphQLService
  private var launches: [Launch]?

  var router: LaunchesRoutering?

  /// Dependencies for this interactor are injected in init
  init(services: GraphQLService, presenter: LaunchesPresentable) {
    self.services = services
    self.presenter = presenter
    self.presenter.listener = self
    router = LaunchesRouter(interactor: self, viewController: presenter)
  }

  func load() {
    getLaunches()
  }

  /// Gets the list of Launches
  private func getLaunches() {
    presenter.showActivityIndicator()

    // sets the query parameters
    let queryList = LaunchListQuery(limit: viewModel.pagination.limit,
                                    offset: viewModel.pagination.offset,
                                    order: viewModel.pagination.order)

    // API call on background thread to prevent UI block
    services.fetch(query: queryList) { [weak self] result in
      // Memory leaks are prevented using weak self here

      // The result is returned on the main thread, where the UI tasks can be done

      self?.presenter.hideActivityIndicator()
      switch result {
      case .success(let graphQLResult):
        // On success if the launches data exits the display it
        if let launches = graphQLResult.data?.launches {
          self?.launches = launches.compactMap {$0}
          let items = launches.compactMap { launch -> LaunchCellViewModel in
            guard let launch = launch else { fatalError(LaunchListDataError.noLaunchData.localizedDescription) }
            return LaunchCellViewModel(launch: launch)
          }

          self?.updateItems(items)
        } else {
          // No launch data so the error, so that the user is not left in limbo state
          self?.presenter.show(error:LaunchListDataError.noLaunchList.localizedDescription)
        }

      case .failure(let error):
        // Error case, update the user
        self?.presenter.show(error: error.localizedDescription)
      }
    }
  }

  private func updateItems(_ items: [LaunchCellViewModel]) {
    viewModel.items = items
    presenter.show(launches: viewModel)
  }
}

// MARK :- LaunchesPresentableListener
/// Listen to the actions done by the user on the presenter/ view controller
extension LaunchesInteractor: LaunchesPresentableListener {
  func showDetails(id: LaunchID) {
    if let launch = getLauch(by: id) {
      router?.routeToLaunchDetails(model: LaunchDetailsViewModel(launch: launch))
    } else {
      router?.routeToLaunchDetails(id: id)
    }
  }

  private func getLauch(by id: LaunchID) -> Launch? {
    return launches?.first { $0.id == id }
  }
}
