//
//  RootViewController.swift
//  Launches
//
//  Created by Firoz Khursheed on 10/10/21.
//

import UIKit

/// The root controller/ screen. All the other screen can be loaded on this. This way we can change the first screen or home screen any time with ease
class RootViewController: UIViewController {
  private let launchesBuilder = LaunchesBuilder()

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    
    let launchesInteractor = launchesBuilder.build() // A shortcut has been taken for the demo app and the first screen is loaded from here.
    routeToLaunches()
    launchesInteractor.load()
  }

  private func routeToLaunches() {
    guard let vc = launchesBuilder.viewControler else {
      fatalError("Launches ViewController is missing")
    }

    vc.modalPresentationStyle = .fullScreen
    present(vc, animated: false, completion: nil)
  }
}
