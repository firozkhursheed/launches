//
//  Constants.swift
//  Launches
//
//  Created by Firoz Khursheed on 10/10/21.
//

import Foundation

// The hard coding are removed and can be accessed from the respective enum. This zero down on typing errors and gives confidence in any kind of updates, and makes it a single place change
enum AppLink: String {
  case scheme = "com.launches"
}

enum ConstantStrings: String {
  case noImage = "https://www.ncenet.com/wp-content/uploads/2020/04/No-image-found.jpg"
  case sqlLiteFileName = "launches.sqlite"
}
