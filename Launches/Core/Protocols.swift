//
//  Protocols.swift
//  Launches
//
//  Created by Firoz Khursheed on 11/10/21.
//

import Foundation

// Common place to declare protocols which would be used throught the app

protocol ViewControllerable: AnyObject {}

/// A protocol to auto generate the testable code
protocol AutoMockable {}
