//
//  ApolloNetwork.swift
//  Launches
//
//  Created by Firoz Khursheed on 10/10/21.
//

import Foundation
import Apollo
import ApolloSQLite

typealias GraphQLService = ApolloClient // This is done so that if one is required to move away form Apollo to some other client it can be done easily.
// Additinally these would be governed by protocols, which would solve 2 things -
// 1. Switching to a new service layer would not break any existing code, and this would serve as a code creation template
// 2. The networking services code would be testable


public class Network {
  static let shared = Network()

  private(set) lazy var apollo: ApolloClient = {

    // Create sqlLite store, this would be used to cache the data locally for offline usage
    let documentsPath = NSSearchPathForDirectoriesInDomains(
      .documentDirectory,
      .userDomainMask,
      true).first!
    let documentsURL = URL(fileURLWithPath: documentsPath)
    let sqliteFileURL = documentsURL.appendingPathComponent(ConstantStrings.sqlLiteFileName.rawValue)
    let sqliteCache = try! SQLiteNormalizedCache(fileURL: sqliteFileURL)
    let store = ApolloStore(cache: sqliteCache)

    // Create NetworkTransport, passes request through a chain of interceptors that can do work both before and after going to the network
    let client = URLSessionClient()
    let provider = NetworkInterceptorProvider(store: store, client: client)
    let url = URL(string: "https://api.spacex.land/graphql")!  // TODO: This would be handled by environment variable, where depending upon the build, prod or stagin, appropriate url would be loaded

    let requestChainTransport = RequestChainNetworkTransport(interceptorProvider: provider,
                                                             endpointURL: url)

    // Remember to give the store you already created to the client so it
    // doesn't create one on its own
    return ApolloClient(networkTransport: requestChainTransport,
                        store: store)
  }()
}

/// Copied from Apollo documentation https://www.apollographql.com/docs/ios/initialization/#advanced-client-creation
struct NetworkInterceptorProvider: InterceptorProvider {

  // These properties will remain the same throughout the life of the `InterceptorProvider`, even though they
  // will be handed to different interceptors.
  private let store: ApolloStore
  private let client: URLSessionClient

  init(store: ApolloStore,
       client: URLSessionClient) {
    self.store = store
    self.client = client
  }

  func interceptors<Operation: GraphQLOperation>(for operation: Operation) -> [ApolloInterceptor] {
    return [
      MaxRetryInterceptor(),
      CacheReadInterceptor(store: self.store),
      NetworkFetchInterceptor(client: self.client),
      ResponseCodeInterceptor(),
      JSONResponseParsingInterceptor(cacheKeyForObject: self.store.cacheKeyForObject),
      AutomaticPersistedQueryInterceptor(),
      CacheWriteInterceptor(store: self.store)
    ]
  }
}
