//
//  DeeplinkManager.swift
//  Launches
//
//  Created by Firoz Khursheed on 10/10/21.
//

import UIKit

/// The deeplinks library manages any type of deeplinks
final class DeeplinkManager {

  static var launchDetailsBuilder: LaunchDetailsBuilder? // TODO: Make this generic And also remove this reference to prevent any kind of leak

  static func redirect(to module: String, with parameters: [String: String]) {
    switch module {
    case "launch":
      if let id = parameters["id"] {
        launchDetails(id: id)
      } else {
        assertionFailure("ID is missing from the parameters \(parameters)")
      }

    default:
      assertionFailure("Deeplink redirect Module \(module) not found")
    }
  }

  /// Deeplinks to the Launch Details and uses ID to open the screen
  // TODO: when multple different screens can be opened, then clean this code from here.
  private class func launchDetails(id: LaunchID) {
    launchDetailsBuilder = LaunchDetailsBuilder()
    let launchDetailsInteractor = launchDetailsBuilder?.build()

    guard let vc = launchDetailsBuilder?.viewControler else {
      fatalError("LaunchDetails ViewController is missing")
    }

    topmostViewController()?.present(vc, animated: true, completion: nil)
    launchDetailsInteractor?.load(id: id)
  }

  private class func topmostViewController() -> UIViewController? {
    if var topController = UIApplication.shared.keyWindow?.rootViewController { // TODO: fix deprecation
      while let presentedViewController = topController.presentedViewController {
        topController = presentedViewController
      }
      return topController
    }

    return nil // TODO: Handle failure case
  }
}
