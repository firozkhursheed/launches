//
//  Typealias.swift
//  Launches
//
//  Created by Firoz Khursheed on 10/10/21.
//

import Foundation

// Single place to put the type alias for the app, this would prevent creating multiple different alias for same thing.

typealias LaunchID = String
typealias Launch = LaunchListQuery.Data.Launch
