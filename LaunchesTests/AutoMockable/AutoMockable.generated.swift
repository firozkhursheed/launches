// Generated using Sourcery 1.6.0 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT
// swiftlint:disable line_length
// swiftlint:disable variable_name

import Foundation
#if os(iOS) || os(tvOS) || os(watchOS)
@testable import Launches
import UIKit
#elseif os(OSX)
import AppKit
#endif
















class LaunchDetailsPresentableMock: LaunchDetailsPresentable {
    var listener: LaunchDetailsPresentableListener?

    //MARK: - show

    var showDetailsCallsCount = 0
    var showDetailsCalled: Bool {
        return showDetailsCallsCount > 0
    }
    var showDetailsReceivedDetails: LaunchDetailsViewModel?
    var showDetailsReceivedInvocations: [LaunchDetailsViewModel] = []
    var showDetailsClosure: ((LaunchDetailsViewModel) -> Void)?

    func show(details: LaunchDetailsViewModel) {
        showDetailsCallsCount += 1
        showDetailsReceivedDetails = details
        showDetailsReceivedInvocations.append(details)
        showDetailsClosure?(details)
    }

    //MARK: - show

    var showErrorCallsCount = 0
    var showErrorCalled: Bool {
        return showErrorCallsCount > 0
    }
    var showErrorReceivedError: String?
    var showErrorReceivedInvocations: [String] = []
    var showErrorClosure: ((String) -> Void)?

    func show(error: String) {
        showErrorCallsCount += 1
        showErrorReceivedError = error
        showErrorReceivedInvocations.append(error)
        showErrorClosure?(error)
    }

}
class LaunchDetailsPresentableListenerMock: LaunchDetailsPresentableListener {

}
class LaunchesInteractableMock: LaunchesInteractable {
    var router: LaunchesRoutering?

}
class LaunchesPresentableMock: LaunchesPresentable {
    var listener: LaunchesPresentableListener?

    //MARK: - show

    var showLaunchesCallsCount = 0
    var showLaunchesCalled: Bool {
        return showLaunchesCallsCount > 0
    }
    var showLaunchesReceivedLaunches: LaunchesViewModel?
    var showLaunchesReceivedInvocations: [LaunchesViewModel] = []
    var showLaunchesClosure: ((LaunchesViewModel) -> Void)?

    func show(launches: LaunchesViewModel) {
        showLaunchesCallsCount += 1
        showLaunchesReceivedLaunches = launches
        showLaunchesReceivedInvocations.append(launches)
        showLaunchesClosure?(launches)
    }

    //MARK: - show

    var showErrorCallsCount = 0
    var showErrorCalled: Bool {
        return showErrorCallsCount > 0
    }
    var showErrorReceivedError: String?
    var showErrorReceivedInvocations: [String] = []
    var showErrorClosure: ((String) -> Void)?

    func show(error: String) {
        showErrorCallsCount += 1
        showErrorReceivedError = error
        showErrorReceivedInvocations.append(error)
        showErrorClosure?(error)
    }

    //MARK: - showActivityIndicator

    var showActivityIndicatorCallsCount = 0
    var showActivityIndicatorCalled: Bool {
        return showActivityIndicatorCallsCount > 0
    }
    var showActivityIndicatorClosure: (() -> Void)?

    func showActivityIndicator() {
        showActivityIndicatorCallsCount += 1
        showActivityIndicatorClosure?()
    }

    //MARK: - hideActivityIndicator

    var hideActivityIndicatorCallsCount = 0
    var hideActivityIndicatorCalled: Bool {
        return hideActivityIndicatorCallsCount > 0
    }
    var hideActivityIndicatorClosure: (() -> Void)?

    func hideActivityIndicator() {
        hideActivityIndicatorCallsCount += 1
        hideActivityIndicatorClosure?()
    }

    //MARK: - present

    var presentVcCallsCount = 0
    var presentVcCalled: Bool {
        return presentVcCallsCount > 0
    }
    var presentVcReceivedVc: UIViewController?
    var presentVcReceivedInvocations: [UIViewController] = []
    var presentVcClosure: ((UIViewController) -> Void)?

    func present(vc: UIViewController) {
        presentVcCallsCount += 1
        presentVcReceivedVc = vc
        presentVcReceivedInvocations.append(vc)
        presentVcClosure?(vc)
    }

}
class LaunchesPresentableListenerMock: LaunchesPresentableListener {

    //MARK: - showDetails

    var showDetailsIdCallsCount = 0
    var showDetailsIdCalled: Bool {
        return showDetailsIdCallsCount > 0
    }
    var showDetailsIdReceivedId: LaunchID?
    var showDetailsIdReceivedInvocations: [LaunchID] = []
    var showDetailsIdClosure: ((LaunchID) -> Void)?

    func showDetails(id: LaunchID) {
        showDetailsIdCallsCount += 1
        showDetailsIdReceivedId = id
        showDetailsIdReceivedInvocations.append(id)
        showDetailsIdClosure?(id)
    }

}
class LaunchesRouteringMock: LaunchesRoutering {

    //MARK: - routeToLaunchDetails

    var routeToLaunchDetailsIdCallsCount = 0
    var routeToLaunchDetailsIdCalled: Bool {
        return routeToLaunchDetailsIdCallsCount > 0
    }
    var routeToLaunchDetailsIdReceivedId: LaunchID?
    var routeToLaunchDetailsIdReceivedInvocations: [LaunchID] = []
    var routeToLaunchDetailsIdClosure: ((LaunchID) -> Void)?

    func routeToLaunchDetails(id: LaunchID) {
        routeToLaunchDetailsIdCallsCount += 1
        routeToLaunchDetailsIdReceivedId = id
        routeToLaunchDetailsIdReceivedInvocations.append(id)
        routeToLaunchDetailsIdClosure?(id)
    }

    //MARK: - routeToLaunchDetails

    var routeToLaunchDetailsModelCallsCount = 0
    var routeToLaunchDetailsModelCalled: Bool {
        return routeToLaunchDetailsModelCallsCount > 0
    }
    var routeToLaunchDetailsModelReceivedModel: LaunchDetailsViewModel?
    var routeToLaunchDetailsModelReceivedInvocations: [LaunchDetailsViewModel] = []
    var routeToLaunchDetailsModelClosure: ((LaunchDetailsViewModel) -> Void)?

    func routeToLaunchDetails(model: LaunchDetailsViewModel) {
        routeToLaunchDetailsModelCallsCount += 1
        routeToLaunchDetailsModelReceivedModel = model
        routeToLaunchDetailsModelReceivedInvocations.append(model)
        routeToLaunchDetailsModelClosure?(model)
    }

}
class LaunchesViewControllableMock: LaunchesViewControllable {

    //MARK: - present

    var presentVcCallsCount = 0
    var presentVcCalled: Bool {
        return presentVcCallsCount > 0
    }
    var presentVcReceivedVc: UIViewController?
    var presentVcReceivedInvocations: [UIViewController] = []
    var presentVcClosure: ((UIViewController) -> Void)?

    func present(vc: UIViewController) {
        presentVcCallsCount += 1
        presentVcReceivedVc = vc
        presentVcReceivedInvocations.append(vc)
        presentVcClosure?(vc)
    }

}
