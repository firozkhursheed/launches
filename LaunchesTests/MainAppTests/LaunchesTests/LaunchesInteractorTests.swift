//
//  LaunchesInteractorTests.swift
//  LaunchesTests
//
//  Created by Firoz Khursheed on 11/10/21.
//

import XCTest
@testable import Launches

final class LaunchesInteractorTests: XCTestCase {

  private var interactor: LaunchesInteractor!

  private var router: LaunchesRouteringMock!
  private var presenter: LaunchesPresentableMock!

  override func setUp() {
    super.setUp()

    presenter = LaunchesPresentableMock()
    router = LaunchesRouteringMock()

    interactor = LaunchesInteractor(services: Network.shared.apollo, presenter: presenter) // TODO create Mock for Apollo services

    interactor.router = router
  }

  func test_whenInteractorLoad_onSuccess_presentShowLaunches() {
    // Given
    XCTAssertEqual(presenter.showActivityIndicatorCallsCount, 0)
    XCTAssertEqual(presenter.showLaunchesCallsCount, 0)

    // When
    interactor.load()
    wait(for: 5) // TODO: Remove this after Apollo mock

    // Then
    XCTAssertEqual(presenter.showActivityIndicatorCallsCount, 1)
    XCTAssertEqual(presenter.showLaunchesCallsCount, 1)
  }

  func test_whenInteractorLoad_onFailure_presentShowError() {
    // Given
    XCTAssertEqual(presenter.showActivityIndicatorCallsCount, 0)
    XCTAssertEqual(presenter.showLaunchesCallsCount, 0)
    XCTAssertEqual(presenter.showErrorCallsCount, 0)

    // When
    interactor.load()
    // TODO: Simulate failure after Apollo mocks

    // Then
    XCTAssertEqual(presenter.showActivityIndicatorCallsCount, 1)
    XCTAssertEqual(presenter.showLaunchesCallsCount, 0)
  }

  func test_onShowDetails_forUnknownID_routeToLaunchDetailsID() {
    // Given
    XCTAssertEqual(router.routeToLaunchDetailsIdCallsCount, 0)

    // When
    interactor.showDetails(id: "123")

    // Then
    XCTAssertEqual(router.routeToLaunchDetailsIdCallsCount, 1)
  }
}
