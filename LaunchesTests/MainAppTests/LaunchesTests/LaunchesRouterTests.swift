//
//  LaunchesRouterTests.swift
//  LaunchesTests
//
//  Created by Firoz Khursheed on 11/10/21.
//

import XCTest
@testable import Launches

final class LaunchesRouterTests: XCTestCase {

  private var router: LaunchesRouter!

  private var interactor: LaunchesInteractableMock!
  private var viewController: LaunchesViewControllableMock!

  override func setUp() {
    super.setUp()

    interactor = LaunchesInteractableMock()
    viewController = LaunchesViewControllableMock()

    router = LaunchesRouter(interactor: interactor,
                            viewController: viewController)
  }

  func test_routeToLaunchDetails() {
    // Given
    XCTAssertEqual(viewController.presentVcCallsCount, 0)

    // When
    router.routeToLaunchDetails(id: "LaunchID")

    // Then
    XCTAssertEqual(viewController.presentVcCallsCount, 1)
  }
}
