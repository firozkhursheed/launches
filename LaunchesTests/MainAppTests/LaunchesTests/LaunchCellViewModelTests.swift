//
//  LaunchCellViewModelTests.swift
//  LaunchesTests
//
//  Created by Firoz Khursheed on 11/10/21.
//

import XCTest
@testable import Launches

final class LaunchCellViewModelTests: XCTestCase {

  var sut: LaunchCellViewModel!
  var launch: Launch!

  override func setUp() {
    super.setUp()

    launch = mockLaunchData()
    sut = LaunchCellViewModel(launch: launch)
  }

  func test_viewModel_mapping_with_launch() {
    XCTAssertEqual(sut.id, launch.id)
    XCTAssertEqual(sut.title, launch.missionName)
    XCTAssertEqual(sut.imageSource.absoluteString, launch.links!.flickrImages![0]!)
  }

  private func mockLaunchData() -> Launch {
    Launch(id: "launch_id",
           links: Launch.Link( flickrImages: ["flickr_link_1"]),
           missionName: "mission_name")
  }
}
